======================================== RESPONSIVE DESIGN ========================================

 There are three characteristics that define responsive design:
 1. A Flexible grid-based layout.
 2. Media queries (CSS3).
 3. Images that resize.

FLOATS

Never intended to be used as a layout tool, but was used as a hack to use it like so.
- Features rows and cells.
- Rows clear the floats on the cells (if you float, you must clear).
- Source ordering determines display, though some rearrengement is possible, but limited.
- Major disadvantage: equal column heights.

[class*="col-"] {} // attribute selector, anywhere within the markup that has class="col-" as a wildcard
will be affected by that declaration.

Flexbox

·The first layout elements - but not designed to layout whole web pages.
· Features flex-containers and flex-items. Both are required to work.
· Excels at vertical centering and equal heights.
· Easy to reorder

Disadvantages
- Was not designed to be locked down for layouts! Works in one dimension only (image like 1 continuous row).
- Browser support and syntax is challenging.

Flexbox terminology:

There always must be a parent and a child. The parent can have different values associated, we can think of it
as a row, or either as a column. So if the flex container is thought as a column, then the flex items, or children
stack on top of each other, and then we can talk about the MAIN AXIS and the CROSS AXIS.

The main axis matches the direction of that flex container
Main axis
- column: vertical
- row: horizontal

However the most common way to think about flexbox is in rows.

row
-> main axis
|
V cross axis

And the flexbox items will just sit next to each other.

Example: 
ul {
  display: flex;
}

Flexbox properties

Parent: (Container)
  display: flex | inline-flex;

  flex-direction: row | row-reverse | column | column-reverse

  flex-wrap: wrap | nowrap | wrap-reverse

  flex-flow: (shorthand for flex-direction and flex-wrap)

  justify-content (main-axis): flex-start | flex-end | center | space-between | space-around | space-evenly;
  - Determines the distribution of the flex-items within the container on the main axis, also the remaining
  space between the items. (flex-start is default which pushes all the items to the start of the main axis).
  In a row wrap, flex-start pushes all the items to the left.

  align-items (cross-axis - adjust to individual sizes): flex-start | flex-end | center | baseline | stretch;
  - Defaults to stretch

  align-content (cross-axis adjust to largest item): flex-start | flex-end | center | stretch | space-between | space-around;


Children (Flex Items)
// What we can change here is ordering and how wide they are. Also change their size over dimensions

  order: <integer>; // default value of 1.

  flex-grow: <number>;

  flex-shrink: <number>;

  // The number for both of the properties above has to do with how fast a box expands or it shrinks in space
  // By default all items expand at the same rate relative to each other.

  flex-basis <length> | auto;
  // flex-bases is "width" but sorta, because is flexible, it is a relative sort of number.
  // If there are other artifacts like margin or padding etc, the flex-basis will always try to 
  // be as close as its input. DO NOT use width in the flex children ever, use flex-basis.

  // these 3 things are usually written in the shorthand because they are almost Never
  // used alone.

  flex: shorthand for grow, shrink and basis (default: 0 1 auto);

  align-self: overrides alignment set on parent;


